<?php


//Classes
use App\Http\Controllers\PokerrrrTableController;
use App\Http\Controllers\UserDashboardDataController;
use App\Http\Controllers\UserClubDashboardDataController;
use App\Http\Controllers\ClubController;
use App\Http\Controllers\ClubDashboardDataController;
use App\Http\Controllers\ClubTableUploadController;
use App\Http\Controllers\ClubTableController;

//Models
use App\Models\User;
use App\Models\PokerrrrTable;
use App\Models\PokerrrrTablePlayer;
use App\Models\ClubUser;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

use Carbon\Carbon;
use Carbon\CarbonImmutable;

use League\Csv\Reader;
use League\Csv\Statement;

//\Debugbar::disable();


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$userID = 5;

//Route::middleware('auth:sanctum')->group(function () {

    //User Dashboard Data
    Route::get('/users/{userId}/dashboard-data', [UserDashboardDataController::class, 'index']);

    //User Clubs
    Route::get('/users/{userId}/clubs', function ($userId) {
        $user = User::find($userId);
        $clubs = $user->clubs()->select('clubs.id', 'name')->get();
        return $clubs->toJson();
    });

    Route::get('/users/{userId}/clubs/{clubId}', function ($userId, $clubId) {
        $user = User::find($userId);
        $club = $user->clubs()->find($clubId);

        return response()->json([
            'club_name' => $club->name,
            'fullname'      => $club->pivot->fullname,
            'email'     => $club->pivot->email,
            'phone'     => $club->pivot->phone,
            'cap'       => $club->pivot->cap,
            'account'   => $club->pivot->account,
            'message'   => $club->message,
        ]);
    });

    //User Club Dashboard Data
    Route::get('/users/{userId}/clubs/{clubId}/dashboard-data', UserClubDashboardDataController::class);


    Route::get('/clubs', [ClubController::class, 'index']);
    Route::get('/clubs/{clubId}', [ClubController::class, 'show']);


    //Club Staff Panel Dashboard Data
    Route::get('/clubs/{clubId}/dashboard-data', ClubDashboardDataController::class);

    //Table Upload
    Route::post('/clubs/{clubId}/table-upload', ClubTableUploadController::class);
    //Table Overview
    Route::get('/clubs/{clubId}/tables', [ClubTableController::class, 'index']);
    
    /*function (Request $request) {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        
        $request->validate([
            'csvFile' => 'required|file|mimes:csv,txt|max:10'
        ]);

        if (!$request->file('csvFile')->isValid()) {
            throw ValidationException::withMessages(['error' => 'not intact']);
        }

        $file = $request->file('csvFile');
        $csvReader = Reader::createFromPath($file->path(), 'r');
        $csvReader->setHeaderOffset(0);
        $csvReader->setDelimiter(';');
        $records = Statement::create()->process($csvReader);
        foreach ($records->getRecords() as $record) {

        $records->fetchColumn()
        return response()->json(['count' => count($records)]);
        //$records = $csv->getRecords();

        /*$tableData = []; //pokerrrrTables

        foreach ($records as $offset => $record) {
            if ($offset === 1) {
                $tableData['game_code'] = $record->
            }
        }
        */
        


        //$request->file('file')->storeAs('tables', $request->file('file')->getClientOriginalName() . '_' . uniqid());
        //return response()->json(['headers' => $csv->getHeader(), 'game_code' => $gameCode]);
    //});
//});