<?php

use Illuminate\Support\Facades\Route;
use League\Csv\Reader;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/csv', function () {
    $csv = Reader::createFromPath('../csv/test.csv', 'r');
    $csv->setDelimiter(';');
    $csv->setHeaderOffset(0);
    $header_offset = $csv->getHeaderOffset(); //returns 0
    $header = $csv->getHeader();
    dd($header);
});
