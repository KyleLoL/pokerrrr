<?php

namespace Tests\Feature;

use App\Exceptions\InvalidTableTypeException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use App\Imports\PokerrrrCsvImport;
use Database\Seeders\PokerrrrGameTypeSeeder;
use Maatwebsite\Excel\Facades\Excel;

//models
use App\Models\User;
use App\Models\Club;
use App\Models\ClubUser;
use App\Models\PokerrrrTable;
use App\Models\PokerrrrTablePlayer;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class FileValidationTest extends TestCase
{
    use DatabaseMigrations;
    protected $clubId;

    /**
     * @test
     * @dataProvider failingValidationInputs
    */
    public function test_validation_errors($ruleName, $payload)
    {
        $this->seed(PokerrrrGameTypeSeeder::class);
        $club = Club::factory()->create();

        $import = new PokerrrrCsvImport($club->id);

        $validatingData = $this->setupPayload($payload, $club);
        $validateRows = $import->validateRows($validatingData);


        $this->assertTrue($validateRows->errors()->has($ruleName));
    }
    public function failingValidationInputs()
    {
        return [
            'GameType not found' => [
                '0.GameType',
                ['GameType' => 'Not Real Game Type']
            ],
            'Gamecode fails because of duplicate' => [
                '0.GameCode',
                function () {
                    $table = PokerrrrTable::factory()->create();
                    return ['GameCode' => $table->game_code];
                },
            ],
            'BigBlind fails because negative number' => [
                '0.BigBlind',
                ['BigBlind' => -1]
            ],
            'Hands fail because negative number' => [
                '0.Hands',
                ['Hands' => -1]
            ],
            'ID fails because it does not exist' => [
                '0.ID',
                function ($club) {
                    $clubUser = ClubUser::factory()
                        ->for($club)
                        ->create();

                    return ['ID' => '#ASASA'];
                }
            ]
        ];
    }

    /**
     * @test
    */
    public function test_validation_passes()
    {
        $this->seed(PokerrrrGameTypeSeeder::class);
        $club = Club::factory()->create();
        $table = PokerrrrTable::factory()->for($club)->create();
        $tablePlayers = PokerrrrTablePlayer::factory()->for($table)->count(2)->create();
        $clubUsers = ClubUser::factory()->for($club)->count(2)->create();

        $import = new PokerrrrCsvImport($club->id);

        $validatingData = [
            0 => [
                'Rank'      => 1,
                'Player'    => $tablePlayers[0]->player_name,
                'ID'        => '#'.$clubUsers[0]->pokerrrr_id,
                'Hands'     => $tablePlayers[0]->hands_played,
                'Profit'    => $tablePlayers[0]->profit,
                'BuyIn'     => $tablePlayers[0]->buy_in_total,
                'Tips'      => $tablePlayers[0]->tips_paid,
                'GameCode'  => 'notadfas',
                'GameType'  => $table->game_type_name,
                'BigBlind'  => $table->big_blind,
                'TotalTips' => $table->total_tips
            ],
            1 => [
                'Rank'      => 1,
                'Player'    => $tablePlayers[1]->player_name,
                'ID'        => '#'.$clubUsers[1]->pokerrrr_id,
                'Hands'     => $tablePlayers[1]->hands_played,
                'Tips'      => $tablePlayers[0]->tips_paid,
                'Profit'    => $tablePlayers[1]->profit,
                'BuyIn'     => $tablePlayers[1]->buy_in_total,
            ]
        ];
        $validateRows = $import->validateRows($validatingData);
        $this->assertFalse($validateRows->fails());
    }

    /**
     * @test
     */
    public function test_laravel_excel_file_imported()
    {
        Excel::fake();
        $file = new UploadedFile(
            base_path('tests/Feature/assets/files/1.csv'),
            '1.csv',
            'text/csv',
            null,
            true
        );
        $club = Club::factory()->create();
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('api/clubs/'.$club->id.'/table-upload', [
            'csvFile' => $file
        ]);
        Excel::assertImported('1.csv');
    }
    public function test_invalid_game_type_throws_correct_exception()
    {
        $this->withoutExceptionHandling();
        $this->seed(PokerrrrGameTypeSeeder::class);

        $file = new UploadedFile(
            base_path('tests/Feature/assets/files/invalid_game_type.csv'),
            'invalid_game_type.csv',
            'text/csv',
            null,
            true
        );
        $club = Club::factory()->create();
        $clubUser = ClubUser::factory()->for($club)->create(['pokerrrr_id' => 'AAAAA']);
        $this->expectException(InvalidTableTypeException::class);
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('api/clubs/' . $club->id . '/table-upload', [
            'csvFile' => $file
        ]);
    }
    /**
     * @test
     */
    public function test_pokerrrr_csv_saved_to_database()
    {
        $this->seed(PokerrrrGameTypeSeeder::class);

        $file = new UploadedFile(
            base_path('tests/Feature/assets/files/1.csv'),
            '1.csv',
            'text/csv',
            null,
            true
        );
        $club = Club::factory()->create();
        ClubUser::factory()->for($club)->create(['pokerrrr_id' => 'AAAAA']);
        ClubUser::factory()->for($club)->create(['pokerrrr_id' => 'AAAAB']);
        ClubUser::factory()->for($club)->create(['pokerrrr_id' => 'AAAAC']);
        ClubUser::factory()->for($club)->create(['pokerrrr_id' => 'AAAAD']);
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('api/clubs/' . $club->id . '/table-upload', [
            'csvFile' => $file
        ]);
        $this->assertDatabaseHas('pokerrrr_tables', [
            'club_id'       => $club->id,
            'game_code'     => '05924840',
            'game_type_id'  => 10,
            'big_blind'     => 5,
            'total_tips'    => 0,
            'hands_played'  => 0
        ]);
        $this->assertDatabaseHas('pokerrrr_table_players', [
            'pokerrrr_id'   => 'AAAAA',
            'player_name'   => 'LauseMerete97',
            'hands_played'  => 30,
            'profit'        => 1410,
            'buy_in_total'  => 1000,
            'tips_paid'     => 0
        ]);
        $response->assertStatus(201);
    }

    /**
     * @test
     */
    public function test_laravel_excel_tournament_file_imported()
    {
        Excel::fake();
        $file = new UploadedFile(
            base_path('tests/Feature/assets/files/Tournament.csv'),
            'Tournament.csv',
            'text/csv',
            null,
            true
        );
        $club = Club::factory()->create();
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('api/clubs/' . $club->id . '/table-upload', [
            'csvFile' => $file
        ]);
        Excel::assertImported('Tournament.csv');
    }
    /**
     * @test
     */
    public function test_pokerrrr_csv_tournament_saved_to_database()
    {
        $this->withoutExceptionHandling();
        $this->seed(PokerrrrGameTypeSeeder::class);

        $file = new UploadedFile(
            base_path('tests/Feature/assets/files/Tournament.csv'),
            'Tournament.csv',
            'text/csv',
            null,
            true
        );
        $club = Club::factory()->create();
        ClubUser::factory()->for($club)->create(['pokerrrr_id' => 'VAY51']);
        ClubUser::factory()->for($club)->create(['pokerrrr_id' => 'J5QWA']);
        ClubUser::factory()->for($club)->create(['pokerrrr_id' => 'S54VN']);
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->post('api/clubs/' . $club->id . '/table-upload', [
            'csvFile' => $file
        ]);
        $response->dump();
        $this->assertDatabaseHas('pokerrrr_tables', [
            'club_id'       => $club->id,
            'game_code'     => '00622171',
            'game_type_id'  => 5,
            'big_blind'     => 0,
            'buyin_ticket'  => 1000,
            'addon_ticket'  => 0,
            'total_tips'    => 1600,
            'hands_played'  => 0
        ]);
        $this->assertDatabaseHas('pokerrrr_table_players', [
            'pokerrrr_id'   => 'VAY51',
            'player_name'   => 'Forrest Gump',
            'hands_played'  => 130,
            'profit'        => 6900,
            'buy_in_total'  => 1100,
            'tips_paid'     => 100
        ]);
        $this->assertDatabaseHas('pokerrrr_table_players', [
            'pokerrrr_id'   => 'J5QWA',
            'player_name'   => 'Trunchbull',
            'hands_played'  => 185,
            'profit'        => 3700,
            'buy_in_total'  => 1100,
            'tips_paid'     => 100
        ]);
        $this->assertDatabaseHas('pokerrrr_table_players', [
            'pokerrrr_id'   => 'S54VN',
            'player_name'   => 'Hagen',
            'hands_played'  => 32,
            'profit'        => -2200,
            'buy_in_total'  => 2200,
            'tips_paid'     => 200
        ]);
        $response->assertStatus(201);
    }
    
    private function setupPayload($payload, $club)
    {
        return is_callable($payload) ? [0 => $payload($club)] : [0 => $payload];
    }

}

