<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\ClubUser;

class PokerrrrIdExists implements Rule
{
    protected $clubId;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($clubId)
    {
        $this->clubId = $clubId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $count = ClubUser::where('club_id', $this->clubId)
            ->where('pokerrrr_id', '=', str_replace('#', '', $value))
            ->count();
            
        return ($count > 0);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute does not exist.';
    }
}
