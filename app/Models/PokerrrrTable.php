<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PokerrrrTable extends Model
{
    use HasFactory;
    protected $table = 'pokerrrr_tables';
    protected $appends = ['game_type_name'];
    protected $visible = ['id', 'game_code', 'game_type_name', 'big_blind', 'total_tips', 'hands_played'];
    protected $fillable = [
        'club_id', 'game_code',
        'game_type_id', 'big_blind',
        'buyin_ticket', 'addon_ticket',
        'total_tips', 'hands_played'
    ];
    protected $attributes = [
        'total_tips' => 0,
        'big_blind' => 0,
    ];
    public function gameType()
    {
        return $this->hasOne(PokerrrrGameType::class, 'key', 'game_type_id');
    }
    public function club() {
        return $this->belongsTo(Club::class);
    }
    public function pokerrrrTablePlayers() {
        return $this->hasMany(PokerrrrTablePlayer::class);
    }

    public function getGameTypeNameAttribute() {
        return $this->gameType->name;
    }
}
