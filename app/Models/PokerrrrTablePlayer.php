<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PokerrrrTablePlayer extends Model
{
    use HasFactory;
    protected $fillable = [
        'pokerrrr_table_id',
        'pokerrrr_id',
        'player_name',
        'hands_played',
        'profit',
        'buy_in_total',
        'rebuys',
        'addons',
        'tips_paid'
    ];
    protected $hidden = [
        'password',
        'email_verified_at',
        'remember_token',
        'updated_at',
        'laravel_through_key'
    ];
    protected $casts = ['profit' => 'integer'];

    public function pokerrrrTable() {
        return $this->belongsTo(PokerrrrTable::class);
    }
}
