<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PokerrrrId extends Model
{
    use HasFactory;
    protected $fillable = ['pokerrrr_id', 'user_id'];
}
