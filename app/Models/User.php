<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'email_verified_at'
    ];
    protected $hidden = [
        'password',
        'email_verified_at',
        'remember_token',
        'updated_at'
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
        'total_hands' => 'integer',
    ];

    protected $appends = ['global_id'];

    public function getGlobalIdAttribute($key)
    {
        return 'I83A18';
    }
    public function getCurrentWeekProfitAttribute($key)
    {
        return (int) $key ?? 0;
    }
    public function getLastWeekProfitAttribute($key)
    {
        return (int) $key ?? 0;
    }
    public function getLastLastWeekProfitAttribute($key)
    {
        return (int) $key ?? 0;
    }
    public function getCurrentWeekHandsAttribute($key)
    {
        return (int) $key ?? 0;
    }
    public function getLastWeekHandsAttribute($key)
    {
        return (int) $key ?? 0;
    }
    public function getLastLastWeekHandsAttribute($key)
    {
        return (int) $key ?? 0;
    }


    public function pokerrrrTables() {
        return $this->hasManyThrough(
            PokerrrrTable::class,
            Club::class,
            'user_id', // clubs.user_id
            'club_id', // pokerrrr_tables.club_id
            'id', // users.id
            'id', // clubs.id
        );
    }

    public function pokerrrrTablePlayers() {
        return $this->hasManyThrough(
            PokerrrrTablePlayer::class,
            PokerrrrId::class,
            'user_id', // pokerrrr_ids.user_id
            'pokerrrr_id', // pokerrrr_table_players.pokerrrr_id
            'id', // users.id
            'pokerrrr_id', // pokerrrr_ids.pokerrrr_id
        );
    }

    public function clubs() {
        return $this->belongsToMany(Club::class)->withPivot(
        [
            'fullname',
            'email',
            'phone',
            'cap',
            'account'
        ]);
    }
}
