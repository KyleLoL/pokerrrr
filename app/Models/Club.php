<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'name'];
    //protected $hidden = ['pivot_user_id', 'pivot_club_id', 'created_at', 'updated_at'];
    protected $visible = ['id', 'name', 'message'];
    protected $appends = ['message'];


    public function clubMessages() {
        return $this->hasMany(ClubMessage::class);
    }
    public function getMessageAttribute() {
        return $this->clubMessages()->orderBy('created_at')->first();
    }

    public function pokerrrrTables() {
        return $this->hasMany(PokerrrrTable::class);
    }

    public function pokerrrrTablePlayers() {
        return $this->hasManyThrough(
            PokerrrrTablePlayer::class,
            PokerrrrTable::class,
            'club_id', // pokerrrr_tables.club_id
            'pokerrrr_table_id', // pokerrrr_table_players.pokerrrr_table_id
            'id', // clubs.id
            'id', // pokerrrr_tables.id
        );
    }
}
