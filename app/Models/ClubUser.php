<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ClubUser extends Pivot
{
    use HasFactory;
    protected $visible = ['fullname','email','phone','cap','account'];
    public function club()
    {
        return $this->belongsTo(Club::class);
    }
}
