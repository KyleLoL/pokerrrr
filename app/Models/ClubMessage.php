<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClubMessage extends Model
{
    use HasFactory;
    protected $fillable = ['club_id', 'title', 'message'];
    protected $visible = ['title', 'message'];
}
