<?php

namespace App\Http\Controllers;

use App\Models\PokerrrrTable;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class PokerrrrTableController extends Controller
{
    public function show(PokerrrrTable $table) {
        Auth::login(User::find(5));
        $this->authorize('view', $table);
        return $table->toJson();
    }
}
