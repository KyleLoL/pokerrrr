<?php

namespace App\Http\Controllers;

use App\Imports\PokerrrrCsvImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;
use League\Csv\Reader;
use League\Csv\Statement;
use Maatwebsite\Excel\Facades\Excel;


use App\Models\Club;
use App\Models\PokerrrrTable;
use App\Models\PokerrrrGameType;
use App\Models\PokerrrrTablePlayer;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ClubTableUploadController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $clubId)
    {
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        
        $request->validate([
            'csvFile' => 'required|file|mimes:csv,txt|max:10'
        ]);

        if (!$request->file('csvFile')->isValid()) {
            throw ValidationException::withMessages(['error' => 'not intact']);
        }

        $collection = Excel::import(new PokerrrrCsvImport($clubId), $request->file('csvFile'));
        return Response::json([], 201);
    }
}
