<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Models
use App\Models\Club;

class ClubController extends Controller
{
    public function index(Request $request) {
        return Club::all();
    }

    public function show(Request $request, $clubId) {
        $club = Club::find($clubId);

        return $club->toJson();
    }
}
