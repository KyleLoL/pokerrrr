<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Club;

class ClubDashboardDataController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $clubId)
    {
        $club = Club::find($clubId);
        $clubTables = $club->pokerrrrTables();
        $totalTips = $clubTables->sum('total_tips');

        return response()->json([
            'total_tips' => $totalTips,
            'active_members' => 0,
        ]);
    }
}
