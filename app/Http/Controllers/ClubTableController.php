<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Club;
use App\Http\Resources\PokerrrrTableResource;


class ClubTableController extends Controller
{
    public function index(Request $request, $clubId)
    {
        $club = Club::find($clubId);
        $clubTables = $club->pokerrrrTables;
        $clubTables->load('gameType');

        return PokerrrrTableResource::collection($clubTables);
    }
}
