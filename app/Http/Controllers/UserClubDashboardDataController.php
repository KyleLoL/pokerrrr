<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Carbon\CarbonImmutable;

use App\Models\User;

class UserClubDashboardDataController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $userId, $clubId)
    {
        $now = CarbonImmutable::now();
        $week = $request->query('week', 'current');

        if ($week === 'current') {
            $start = $request->query('start', $now->startOfWeek(Carbon::MONDAY)->format('y-m-d'));
            $end = $request->query('end', $now->endOfWeek(Carbon::SUNDAY)->format('y-m-d'));
        } else if ($week === 'last') {
            $start = $request->query('start', $now->startOfWeek(Carbon::MONDAY)->subDays(7)->format('y-m-d'));
            $end = $request->query('end', $now->endOfWeek(Carbon::SUNDAY)->subDays(7)->format('y-m-d'));
        }
        $user = User::select('id')->find($userId);

        $dailyProfits = $user->pokerrrrTablePlayers()
            ->select(DB::raw('
            SUM(profit) as profit,
            WEEKDAY(pokerrrr_table_players.created_at) as day_of_week,
            YEARWEEK(pokerrrr_table_players.created_at, 3) as week_of_year'))
            ->whereHas('pokerrrrTable', function ($q) use ($clubId) {
                $q->where('club_id', $clubId);
            })
            ->whereBetween('pokerrrr_table_players.created_at', [$start, $end])
            ->groupByRaw('day_of_week, week_of_year, laravel_through_key')
            ->orderBy('day_of_week')
            ->get();

        $lastWeekProfit = $user->pokerrrrTablePlayers()
            ->whereHas('pokerrrrTable', function ($q) use ($clubId) {
                $q->where('club_id', $clubId);
            })
            ->whereBetween('pokerrrr_table_players.created_at', [Carbon::parse($start)->subDays(7), Carbon::parse($end)->subDays(7)])
            ->sum('profit');

        $weekTotalHands = $user->pokerrrrTablePlayers()
            ->whereHas('pokerrrrTable', function ($q) use ($clubId) {
                $q->where('club_id', $clubId);
            })
            ->whereBetween('pokerrrr_table_players.created_at', [$start, $end])
            ->sum('hands_played');

        $lastWeekTotalHands = $user->pokerrrrTablePlayers()
            ->whereHas('pokerrrrTable', function ($q) use ($clubId) {
                $q->where('club_id', $clubId);
            })
            ->whereBetween('pokerrrr_table_players.created_at', [Carbon::parse($start)->subDays(7), Carbon::parse($end)->subDays(7)])
            ->sum('hands_played');


        return response()->json([
            'global_id' => $user->global_id,
            'profits' => $dailyProfits,
            'last_week_profit' => (int) $lastWeekProfit,
            'hands' => (int) $weekTotalHands,
            'last_week_hands' => (int) $lastWeekTotalHands
        ]);
    }
}
