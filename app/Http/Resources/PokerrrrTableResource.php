<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PokerrrrTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'game_code' => $this->game_code,
            'date'      => $this->created_at,
            'game_type' => $this->gameType->name,
            'rake'      => $this->total_tips,
            'hands'     => $this->hands_played,
            'big_blind' => $this->big_blind,
        ];
    }
}
