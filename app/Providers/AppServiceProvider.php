<?php

namespace App\Providers;

use App\Rules\PokerrrrIdExists;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('pokerrrr_id_exists', function ($attribute, $value, $parameters, $validator) {
            list($clubId) = $parameters;
            return (new PokerrrrIdExists($clubId))->passes($attribute, $value);
        });
    }
}
