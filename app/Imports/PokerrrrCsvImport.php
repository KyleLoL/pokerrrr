<?php

namespace App\Imports;

use App\Exceptions\InvalidTableTypeException;
use App\Models\PokerrrrTable;
use App\Models\PokerrrrGameType;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;


class PokerrrrCsvImport implements ToCollection, WithHeadingRow, WithCustomCsvSettings
{
    protected $clubId;
    protected $tableType;
    public function __construct($clubId = 0)
    {
        $this->clubId = $clubId;
    }

    private function removeEmoji($text)
    {
        $cleanText = "";

        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $cleanText = preg_replace($regexEmoticons, '', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $cleanText = preg_replace($regexSymbols, '', $cleanText);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $cleanText = preg_replace($regexTransport, '', $cleanText);

        return $cleanText;
    }

    private function getTableType(Collection $row)
    {
        if($row->has(['BuyIn', 'BigBlind'])) {
            return 'Cash';
        }else if($row->has(['Rebuy', 'Prize', 'Addon', 'BuyinTicket', 'AddonTicket'])) {
            return 'Tournament';
        }
        throw new InvalidTableTypeException;
    }
    private function getBuyin(string $tableType, PokerrrrTable $table, Collection $row)
    {
        if($tableType === 'Cash') {
            return $row['BuyIn'];
        }else if($tableType === 'Tournament') {
            return $table->buyin_ticket
                + ($table->buyin_ticket * $row['Rebuy'])
                + ($table->addon_ticket * $row['Addon'])
                + $row['Tips'];
        }

        return 0;
    }

    public function collection(Collection $rows)
    {

        $validation = $this->validateRows($rows->toArray());
        $validation->validate();


        $tableType = null;
        $table = null;
        foreach ($rows as $offset => $row) {
            if($offset === 0) {
                $tableType = $this->getTableType($row);
                $gameTypeKey = PokerrrrGameType::where('name', $row['GameType'])->firstOrFail()->key;
                
                if ($tableType === 'Cash') {
                    $table = PokerrrrTable::create([
                        'club_id'       => $this->clubId,
                        'game_code'     => $row['GameCode'],
                        'game_type_id'  => $gameTypeKey,
                        'big_blind'     => (int) $row['BigBlind'],
                        'total_tips'    => (int) $row['TotalTips']
                    ]);
                } else if($tableType === 'Tournament') {
                    $table = PokerrrrTable::create([
                        'club_id'      => $this->clubId,
                        'game_code'    => $row['GameCode'],
                        'game_type_id' => $gameTypeKey,
                        'buyin_ticket' => (int) $row['BuyinTicket'],
                        'addon_ticket' => (int) $row['AddonTicket'],
                        'total_tips'   => (int) $row['TotalTips']
                    ]);
                }
            }
            if ($tableType === 'Cash') {
                $table->pokerrrrTablePlayers()->create([
                    'pokerrrr_id'   => str_replace('#', '', $row['ID']),
                    'player_name'   => $this->removeEmoji($row['Player']),
                    'hands_played'  => (int) $row['Hands'],
                    'profit'        => (int) $row['Profit'],
                    'buy_in_total'  => (int) $this->getBuyin($tableType, $table, $row),
                    'tips_paid'     => (int) $row['Tips'],
                ]);
            } else if ($tableType === 'Tournament') {
                $table->pokerrrrTablePlayers()->create([
                    'pokerrrr_id'   => str_replace('#', '', $row['ID']),
                    'player_name'   => $this->removeEmoji($row['Player']),
                    'hands_played'  => (int) $row['Hands'],
                    'profit'        => (int) $row['Profit'],
                    'buy_in_total'  => (int) $this->getBuyin($tableType, $table, $row),
                    'tips_paid'     => (int) $row['Tips'],
                    'rebuys'        => (int) $row['Rebuy'],
                    'addons'        => (int) $row['Addon']
                ]);
            }
        }
    } 

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ";"
        ];
    }

    public function validateRows(array $data)
    {
        return Validator::make($data, [
            '0.GameCode' => 'required|string|size:8|unique:pokerrrr_tables,game_code',
            '0.GameType' => 'required|string|exists:pokerrrr_game_types,name',
            '0.BuyinTicket' => 'sometimes|integer|min:0',
            '0.AddonTicket' => 'sometimes|integer|min:0',
            '0.BigBlind' => 'sometimes|integer|min:0',
            '0.TotalTips' => 'present|nullable|integer|min:0',
            '*.Player' => ['required', 'string', 'max:60'],
            '*.ID' => [
                'required',
                'string',
                'starts_with:#',
                'size:6',
                'distinct',
                'pokerrrr_id_exists:'.$this->clubId,
            ],
            '*.Hands' => ['required', 'integer', 'min:0'],
            '*.Profit' => ['required', 'integer'],
            '*.Rebuy' => ['sometimes', 'nullable', 'integer', 'min:0'],
            '*.Prize' => ['sometimes', 'nullable', 'integer', 'min:0'],
            '*.Addon' => ['sometimes', 'nullable', 'integer', 'min:0'],
            '*.BuyIn' => ['sometimes', 'integer', 'min:0'],
            '*.Tips' => ['present', 'nullable', 'integer', 'min:0'],
        ]);
    }
}
