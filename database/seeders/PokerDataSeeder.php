<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Club;
use App\Models\ClubMessage;
use App\Models\PokerrrrId;
use App\Models\PokerrrrTable;
use App\Models\PokerrrrTablePlayer;
use App\Models\PokerrrrGameType;
use Carbon\Carbon;
use Carbon\CarbonImmutable;

class PokerDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pokerId = '#KWBCK'; //default mock pokerrrr ID

        //add user
        $userId = User::create([
            'name' => 'Kyle Aldridge',
            'email' => 'thekylealdridge@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('admin123'),
        ])->id;

        //link user to pokerrrr id
        PokerrrrId::create([
            'pokerrrr_id' => $pokerId,
            'user_id' => $userId,
        ]);

        $clubId = Club::create([
            'user_id' => $userId,
            'name' => 'Donkey Dungeon',
        ])->id;

        $clubId2 = Club::create([
            'user_id' => $userId,
            'name' => 'Poker Pirates',
        ])->id;

        $clubMsg = ClubMessage::create([
            'club_id' => $clubId,
            'title' => 'test title',
            'message' => 'hjgjgjfgf ghf hgf hdfs dfs yjfgjhg',
        ]);
        //Club::find($clubId)->clubMessage()->attach($clubMsg);

        User::find($userId)->clubs()->attach($clubId,
        [
            'pokerrrr_id' => 'NMOPI',
            'fullname' => 'Joe Smith',
            'email' => 'joesmith@gmail.com',
            'phone' => 92282895,
            'account' => 123456789
        ]);
        User::find($userId)->clubs()->attach($clubId2,
        [
            'pokerrrr_id' => 'AIEKD',
            'fullname' => 'Kyle Aldridge',
            'email' => 'joesmitasdfh@gmail.com',
            'phone' => 92282323,
            'account' => 1232456789
        ]);

        //create mock pokerrrr tables
        $tableId = PokerrrrTable::create([
            'club_id' => $clubId,
            'game_code' => '05924831',
            'game_type_id' => 10,
            'big_blind' => 5,
            'total_tips' => 5,
            'hands_played' => 0,
        ])->id;

        $tableId2 = PokerrrrTable::create([
            'club_id' => $clubId,
            'game_code' => '05924833',
            'game_type_id' => 10,
            'big_blind' => 5,
            'total_tips' => 5,
            'hands_played' => 0,
        ])->id;

        $tableId3 = PokerrrrTable::create([
            'club_id' => $clubId2,
            'game_code' => '05924835',
            'game_type_id' => 10,
            'big_blind' => 5,
            'total_tips' => 5,
            'hands_played' => 0,
        ])->id;

        $carbon = CarbonImmutable::now();
        $start = $carbon->startOfWeek(Carbon::MONDAY)->subDays(7);
        $start2 = $carbon->startOfWeek(Carbon::MONDAY)->subDays(14);
        //add player data for above pokerrrr tables
        PokerrrrTablePlayer::create([
            'pokerrrr_table_id' => $tableId,
            'pokerrrr_id' => $pokerId,
            'player_name' => '2 weeks ago',
            'hands_played' => 10,
            'profit' => 250,
            'buy_in_total' => 500,
            'tips_paid' => 5,
            'created_at' => $start2
        ]);

        PokerrrrTablePlayer::create([
            'pokerrrr_table_id' => $tableId,
            'pokerrrr_id' => $pokerId,
            'player_name' => '1 week ago',
            'hands_played' => 20,
            'profit' => 500,
            'buy_in_total' => 500,
            'tips_paid' => 10,
            'created_at' => $start
        ]);

        PokerrrrTablePlayer::create([
            'pokerrrr_table_id' => $tableId,
            'pokerrrr_id' => $pokerId,
            'player_name' => 'current week',
            'hands_played' => 30,
            'profit' => 750,
            'buy_in_total' => 500,
            'tips_paid' => 15,
        ]);

        PokerrrrTablePlayer::create([
            'pokerrrr_table_id' => $tableId3,
            'pokerrrr_id' => $pokerId,
            'player_name' => 'current week',
            'hands_played' => 100,
            'profit' => 1500,
            'buy_in_total' => 500,
            'tips_paid' => 15,
            'created_at' => $start2
        ]);

        PokerrrrTablePlayer::create([
            'pokerrrr_table_id' => $tableId3,
            'pokerrrr_id' => $pokerId,
            'player_name' => 'current week',
            'hands_played' => 50,
            'profit' => 1000,
            'buy_in_total' => 500,
            'tips_paid' => 15,
            'created_at' => $start
        ]);
        

    }
}
