<?php

namespace Database\Seeders;

use App\Models\PokerrrrGameType;
use Illuminate\Database\Seeder;

class PokerrrrGameTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PokerrrrGameType::create(['name' => 'PLO', 'key' => 1]);
        PokerrrrGameType::create(['name' => 'Hi-Lo PLO', 'key' => 2]);
        PokerrrrGameType::create(['name' => '5-Card PLO', 'key' => 3]);
        PokerrrrGameType::create(['name' => '5-Card Hi-Lo PLO', 'key' => 4]);
        PokerrrrGameType::create(['name' => 'NL Hold\'em', 'key' => 5]);
        PokerrrrGameType::create(['name' => 'Short Deck', 'key' => 6]);
        PokerrrrGameType::create(['name' => 'R.O.E.', 'key' => 7]);
        PokerrrrGameType::create(['name' => 'Pineapple OFC', 'key' => 8]);
        PokerrrrGameType::create(['name' => 'Progressive OFC', 'key' => 9]);
        PokerrrrGameType::create(['name' => 'OFC Joker', 'key' => 10]);
    }
}
