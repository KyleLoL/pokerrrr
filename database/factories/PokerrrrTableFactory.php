<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PokerrrrTableFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'club_id'       => $this->faker->randomDigit,
            'game_code'     => $this->faker->regexify('[0-9]{8}'),
            'game_type_id'  => $this->faker->numberBetween(1, 10),
            'big_blind'     => $this->faker->numberBetween(0, 5000),
            'total_tips'    => $this->faker->numberBetween(0, 10000),
            'hands_played'  => $this->faker->numberBetween(1, 10000),
        ];
    }
}
