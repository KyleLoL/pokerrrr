<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\PokerrrrTable;

class PokerrrrTablePlayerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $rebuys = $this->faker->numberBetween(0, 10);
        $table = PokerrrrTable::factory()->create();

        return [
            'pokerrrr_table_id'            => $table->id,
            'pokerrrr_id'                  => $this->faker->unique()->regexify('#[A-Z]{5}'),
            'player_name'                  => $this->faker->userName(),
            'hands_played'                 => $this->faker->numberBetween(1, 3000),
            'profit'                       => $this->faker->numberBetween(-10000, 10000),
            'buy_in_total'                 => ($this->faker->numberBetween(0, 1000) * $rebuys),
            'tips_paid'                    => $this->faker->numberBetween(1, $table->total_tips),
            'rebuys'                       => $rebuys,
            'addons'                       => 0,
        ];
    }
}
