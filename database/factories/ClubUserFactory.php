<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\User;
use App\Models\Club;

class ClubUserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user = User::factory()->create();
        $club = Club::factory()->create();

        return [
            'user_id'       => $user->id,
            'club_id'       => $club->id,
            'pokerrrr_id'   => $this->faker->unique()->regexify('[A-Z]{5}'),
            'fullname'      => $this->faker->firstName.' '.$this->faker->lastName,
            'email'         => $this->faker->unique()->email,
            'cap'           => $this->faker->numberBetween(1,5000),
            'phone'         => str_replace(' ', '', str_replace('-', '', $this->faker->unique()->phoneNumber())),
            'account'       => $this->faker->unique()->numberBetween(100000000, 999999999)
        ];
    }
}
