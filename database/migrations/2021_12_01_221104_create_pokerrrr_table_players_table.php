<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePokerrrrTablePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pokerrrr_table_players', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pokerrrr_table_id');
            $table->string('pokerrrr_id', 6);
            $table->string('player_name');
            $table->unsignedSmallInteger('hands_played');
            $table->mediumInteger('profit');
            $table->unsignedMediumInteger('buy_in_total');
            $table->unsignedSmallInteger('tips_paid');
            $table->unsignedTinyInteger('rebuys')->default(0);
            $table->unsignedTinyInteger('addons')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pokerrrr_table_players');
    }
}
