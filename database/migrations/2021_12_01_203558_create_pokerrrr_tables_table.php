<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePokerrrrTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pokerrrr_tables', function (Blueprint $table) {
            $table->id();
            $table->foreignId('club_id');
            $table->string('game_code', 8);
            $table->foreignId('game_type_id');
            $table->unsignedSmallInteger('big_blind')->nullable()->default(0);
            $table->unsignedMediumInteger('buyin_ticket')->nullable()->default(0);
            $table->unsignedMediumInteger('addon_ticket')->nullable()->default(0);
            $table->unsignedMediumInteger('total_tips')->nullable()->default(0);
            $table->unsignedSmallInteger('hands_played')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pokerrrr_tables');
    }
}
