<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_user', function (Blueprint $table) {
            $table->id();
            $table->foreignId('club_id');
            $table->foreignId('user_id')->default(0)->nullable;
            $table->string('pokerrrr_id', 5);
            $table->string('fullname', 40);
            $table->string('email', 60);
            $table->unsignedMediumInteger('cap')->default(50000);
            $table->string('phone', 15);
            $table->unsignedBigInteger('account')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_user');
    }
}
